#!/usr/bin/env ruby

# ENV Variables
# GROUP_ID
# EMAIL_ADDRESS
# SLACK_OAUTH

require 'slack-ruby-client'
require 'dotenv/load' # If using/testing with .env

# Slack WebClient
Slack.configure do |config|
  config.token = ENV['SLACK_OAUTH']
end

# Create Client
client = Slack::Web::Client.new

# Find User ID
user_id = client.users_lookupByEmail(email: ENV['EMAIL_ADDRESS']).user.id

# Update Group
client.usergroups_users_update(users: user_id, usergroup: ENV['GROUP_ID'])
